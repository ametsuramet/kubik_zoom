import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:kubik_zoom/zoom.dart';

void main() {
  const MethodChannel channel = MethodChannel('kubik_zoom');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    //expect(await Zoom.platformVersion, '42');
  });
}
