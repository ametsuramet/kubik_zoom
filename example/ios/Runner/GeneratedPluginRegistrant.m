//
//  Generated file. Do not edit.
//

// clang-format off

#import "GeneratedPluginRegistrant.h"

#if __has_include(<zoom/ZoomPlugin.h>)
#import <zoom/ZoomPlugin.h>
#else
@import zoom;
#endif

@implementation GeneratedPluginRegistrant

+ (void)registerWithRegistry:(NSObject<FlutterPluginRegistry>*)registry {
  [ZoomPlugin registerWithRegistrar:[registry registrarForPlugin:@"ZoomPlugin"]];
}

@end
